# Build and Run
First create a Git repository with "git init". Then initialize go mod with "go mod init". Then "go get gitlab.com/polyapp-open-source/polyapp" Then run "go run main.go".

# Automated Testing
You should create integration tests to ensure the Do[action-name] function runs as you expect.

You can also create integration tests to ensure the rest of the code works properly by using httptest:  https://golang.org/pkg/net/http/httptest/

After your independent tests pass, you can deploy and integrate the service as described below.

# Deploy
This guide assumes you have already installed Polyapp in the same Google Cloud Project you are using to install your Bot. More information: https://gitlab.com/polyapp-open-source/polyapp/-/blob/master/INSTALL_GUIDE.md

## Option 1: Manual Deployment
https://console.cloud.google.com/

git pull [your Git Repo]

cd [your Git Repo]

./google_cloud_deploy.sh

## Option 2: CI/CD Deployment
If you use Gitlab, please refer to the included .gitlab-ci.yml file and set up the needed environment variables. If you deploy using another service, you can use .gitlab-ci.yml as a reference when creating your own script.

# Integrate
You'll want to call your microservice using the HTTPSCall Bot. That Bot is documented <a href="https://polyapp-public.appspot.com/t/polyapp/Bot/eoUIvRnzeFAwpWXRYUDAgCcsT?ux=wEuFHLqvricbzBxgfdWqwljjH&schema=veKWxEWtCEnCDLknytuUPzMHY&data=wCHhssCsBXvqyHPkMwPwKldnC">here on the public instance</a> and at this relative URL: <code>/t/polyapp/Bot/eoUIvRnzeFAwpWXRYUDAgCcsT?ux=wEuFHLqvricbzBxgfdWqwljjH&schema=veKWxEWtCEnCDLknytuUPzMHY&data=wCHhssCsBXvqyHPkMwPwKldnC</code>

Essentially, you will use the HTTPSCallBot's Bot ID (dplFLIDbLxEpMicZPIZBfRElq) and configure it by adding a BotStaticData which looks something like this: <code>HTTPSCall POST https://jsontoapp-something-uc.a.run.app</code> except your URL will be the Cloud Run URL in your instance. Make sure you use POST.

# Manual Testing
You can now test the entire system by ensuring that a Data which is used by the Task calls your Microservice Bot when expected and it performs the expected work.