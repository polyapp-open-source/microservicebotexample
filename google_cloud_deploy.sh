GOOGLE_CLOUD_PROJECT_ID=$(gcloud config get-value project)
PROJECT_NAME_SIMPLE=financialservicesrendered-bot

gcloud builds submit --tag gcr.io/$GOOGLE_CLOUD_PROJECT_ID/$PROJECT_NAME_SIMPLE --project $GOOGLE_CLOUD_PROJECT_ID
gcloud run deploy $PROJECT_NAME_SIMPLE --image gcr.io/$GOOGLE_CLOUD_PROJECT_ID/$PROJECT_NAME_SIMPLE --platform managed --quiet --region us-central1 --allow-unauthenticated --project $GOOGLE_CLOUD_PROJECT_ID --set-env-vars=GOOGLE_CLOUD_PROJECT=$GOOGLE_CLOUD_PROJECT_ID